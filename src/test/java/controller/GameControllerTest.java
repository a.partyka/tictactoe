package controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.assertj.core.api.Assertions.assertThat;

class GameControllerTest {

    private GameController gameController;

    @BeforeEach
    void setUp() {
        gameController = new GameController();
    }

    @Test
    void shouldReturnFalseWhenUserWantPlayMore() {
        System.setIn(new ByteArrayInputStream("T".getBytes()));

        assertThat(gameController.getExitGameFromConsole()).isFalse();
    }

    @Test
    void shouldReturnTrueWhenUserWantExit() {
        System.setIn(new ByteArrayInputStream("test".getBytes()));

        assertThat(gameController.getExitGameFromConsole()).isTrue();
    }
}