package model;

import controller.GameController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

class GameTest {

    Game game;

    @BeforeEach
    void setUp() {
        GameController gameController = mock(GameController.class);
        when(gameController.prepareWinningFields()).thenCallRealMethod();
        game = new Game(gameController.prepareWinningFields());
    }

    @Test
    void shouldReturnFieldWhenCoordinatesAreCorrect() {
        Player player = new Player(Flag.X);
        String coordinates = "1 1";
        System.setIn(new ByteArrayInputStream(coordinates.getBytes()));

        assertThat(game.getTargetFieldFromConsole(player)).isInstanceOf(Field.class);
    }

    @Test
    void shouldExecuteRoundOnceWhenTargetFieldIsCorrect() {
        Game gameSpy = spy(game);
        Player player = mock(Player.class);
        Field targetField = mock(Field.class);

        doReturn(targetField).when(gameSpy).getTargetFieldFromConsole(any(Player.class));
        when(player.hit(any(Field.class))).thenReturn(true);

        gameSpy.executeRound(player);

        verify(gameSpy, times(1)).getTargetFieldFromConsole(any(Player.class));
        verify(player, times(1)).hit(any(Field.class));
    }
}