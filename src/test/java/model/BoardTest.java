package model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BoardTest {

    private Board board;

    @BeforeEach
    void setUp() {
        board = new Board();
    }

    @Test
    void shouldFindFieldWhenGetCorrectCoordinates() {
        Field field = board.findField(1, 1);

        assertThat(field).isNotNull();
        assertThat(field.getFlag()).isEqualTo(Flag.I);
    }

    @Test
    void shouldReturnNullWhenGetIncorrectCoordinates() {
        Field field = board.findField(4, 1);

        assertThat(field).isNull();
    }

    @Test
    void shouldReturnTrueWhenBoardIsFull() {
        board.getFields().forEach(field -> field.setFlag(Flag.X));

        assertThat(board.isFull()).isTrue();
    }

    @Test
    void shouldReturnFalseWhenBoardIsNotFull() {
        assertThat(board.isFull()).isFalse();
    }
}