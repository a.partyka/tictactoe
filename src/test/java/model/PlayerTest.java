package model;

import controller.GameController;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PlayerTest {

    Player player;

    @BeforeEach
    void setUp() {
        player = new Player(Flag.X);
    }

    @Test
    void shouldHitWhenFieldIsNeutral() {
        Field targetField = new Field(1, 1);
        targetField.setFlag(Flag.I);

        assertThat(player.hit(targetField)).isTrue();
        assertThat(targetField.getFlag()).isEqualTo(Flag.X);
    }

    @Test
    void shouldNotHitWhenFieldIsNotNeutral() {
        Field targetField = new Field(1, 1);
        targetField.setFlag(Flag.O);

        assertThat(player.hit(targetField)).isFalse();
        assertThat(targetField.getFlag()).isEqualTo(Flag.O);
    }

    @Test
    void shouldWinWhenHitsContainsWinningFields() {
        GameController gameController = mock(GameController.class);
        when(gameController.prepareWinningFields()).thenCallRealMethod();

        List<List<Field>> winningFields = gameController.prepareWinningFields();
        List<Field> winningHits = Arrays.asList(new Field(1, 1), new Field(2, 2), new Field(3, 3));
        winningHits.forEach(field -> field.setFlag(Flag.I));
        winningHits.forEach(player::hit);

        assertThat(player.isWinner(winningFields)).isTrue();
    }

    @Test
    void shouldNotWinWhenHitsNotContainsWinningFields() {
        GameController gameController = mock(GameController.class);
        when(gameController.prepareWinningFields()).thenCallRealMethod();

        List<List<Field>> winningFields = gameController.prepareWinningFields();
        List<Field> winningHits = Arrays.asList(new Field(1, 2), new Field(2, 2), new Field(3, 3));
        winningHits.forEach(field -> field.setFlag(Flag.I));
        winningHits.forEach(player::hit);

        assertThat(player.isWinner(winningFields)).isFalse();
    }
}