package model;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Game {

    private final List<List<Field>> WINNING_FIELDS;
    private final String COORDINATES_REGEX = "([1-3])\\s([1-3])";
    private List<Player> players;
    private Board board;
    private boolean endGame;

    public Game(List<List<Field>> winning_fields) {
        WINNING_FIELDS = winning_fields;
        board = new Board();
        players = Arrays.asList(new Player(Flag.X), new Player(Flag.O));
        endGame = false;
    }

    public void play() {
        while (!endGame) {
            for (Player player : players) {
                executeRound(player);

                if (endGame = player.isWinner(WINNING_FIELDS)) {
                    System.out.println("Wygrał gracz " + player.getFlag());
                    break;
                } else if (endGame = board.isFull()) {
                    System.out.println("Remis!");
                    break;
                }
            }
        }
    }

    void executeRound(Player player) {
        board.printBoard();
        boolean fieldIsHit = false;
        while (!fieldIsHit) {
            Field targetField = getTargetFieldFromConsole(player);
            fieldIsHit = player.hit(targetField);
        }
    }

    Field getTargetFieldFromConsole(Player player) {
        Field field = null;
        Pattern coordinatePattern = Pattern.compile(COORDINATES_REGEX);
        Matcher coordinateMatcher;

        while (field == null) {
            System.out.println("Graczu " + player.getFlag() + " Podaj współrzędne celu X Y :");
            Scanner scanner = new Scanner(System.in);
            coordinateMatcher = coordinatePattern.matcher(scanner.nextLine());
            if (coordinateMatcher.matches()) {
                field = board.findField(Integer.valueOf(coordinateMatcher.group(1)), Integer.valueOf(coordinateMatcher.group(2)));
            } else System.out.println("Błędne dane, spróbuj ponownie!");
        }
        return field;
    }
}
