package model;

import com.google.gson.annotations.Expose;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

public class Field {

    @Getter
    @Expose
    private final int X;
    @Getter
    @Expose
    private final int Y;
    @Getter
    @Setter
    Flag flag;

    Field(int x, int y) {
        this.X = x;
        this.Y = y;
    }

    boolean isNeutral() {
        return flag.equals(Flag.I);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Field field = (Field) o;
        return X == field.X &&
                Y == field.Y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(X, Y);
    }

    @Override
    public String toString() {
        return "Field{" +
                "X=" + X +
                ", Y=" + Y +
                '}';
    }
}
