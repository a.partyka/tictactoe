package model;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class Board {

    @Getter
    private List<Field> fields;
    private final int X_AXIS_MAXIMUM_VALUE = 3;
    private final int Y_AXIS_MAXIMUM_VALUE = 3;
    private final String Y_LABEL = "Y" + "\n";
    private final String X_LABEL = "\n" + "   " + "1 2 3  X";

    public Board() {
        fields = prepareFields();
    }

    private List<Field> prepareFields() {
        List<Field> fields = new ArrayList<>();
        for (int x = 1; x <= X_AXIS_MAXIMUM_VALUE; x++) {
            for (int y = 1; y <= Y_AXIS_MAXIMUM_VALUE; y++) {
                Field field = new Field(x, y);
                field.setFlag(Flag.I);
                fields.add(field);
            }
        }
        return fields;
    }

    public void printBoard() {
        System.out.println(Y_LABEL);
        for (int y = Y_AXIS_MAXIMUM_VALUE; y > 0; y--) {
            int rowIndex = y;
            List<Field> rowFields = fields.stream().filter(field -> field.getY() == rowIndex).collect(Collectors.toList());
            System.out.print(rowIndex + "  ");
            rowFields.forEach(field -> System.out.print(field.getFlag() + " "));
            System.out.println();
        }
        System.out.println(X_LABEL);
    }

    public Field findField(int x, int y) {
        return fields.stream().filter(field -> field.getX() == x && field.getY() == y).findFirst().orElse(null);
    }

    public boolean isFull() {
        return fields.stream().noneMatch(Field::isNeutral);
    }
}