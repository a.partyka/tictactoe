package model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

class Player {

    @Getter
    private Flag flag;
    private List<Field> hits;

    Player(Flag flag) {
        this.flag = flag;
        this.hits = new ArrayList<>();
    }

    boolean hit(Field targetField) {

        if (targetField != null && targetField.isNeutral()) {
            targetField.setFlag(flag);
            hits.add(targetField);
            return true;
        }
        System.out.println("Pole jest zajęte przez " + targetField.getFlag());
        return false;
    }

    boolean isWinner(List<List<Field>> winningFields) {
        return winningFields.stream().anyMatch(fields -> hits.containsAll(fields));
    }
}