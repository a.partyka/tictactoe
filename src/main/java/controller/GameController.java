package controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import model.Field;
import model.Game;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

@Slf4j
public class GameController {

    private final List<List<Field>> WINNING_FIELDS;
    private final String WINNING_FIELDS_FILE = "winningFields.json";
    private Game game;
    private boolean exitGame;

    public GameController() {
        WINNING_FIELDS = prepareWinningFields();
        exitGame = false;
    }

    public void run() {
        while (!exitGame) {
            game = new Game(WINNING_FIELDS);
            game.play();
            exitGame = getExitGameFromConsole();
        }
    }

    public boolean getExitGameFromConsole() {
        System.out.println("Jeszcze raz? T/N");
        return !new Scanner(System.in).next().equalsIgnoreCase("t");
    }

    public List<List<Field>> prepareWinningFields() {
        //fajnie jakbys sprobowal zrobic do tego test parametryzowany ktory by sprawdzil poprawnosc wygyrwanych pol :) - dla cwiczen
        try (Reader reader = new FileReader(Objects.requireNonNull(getClass().getClassLoader().getResource(WINNING_FIELDS_FILE)).getFile())) {
            Type listType = new TypeToken<List<List<Field>>>() {
            }.getType();
            return new Gson().fromJson(reader, listType);
        } catch (IOException | NullPointerException e) {
            log.error(e.getMessage());
            System.exit(-1);
        }
        return null;
    }
}