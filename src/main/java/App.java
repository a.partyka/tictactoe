import controller.GameController;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class App {

    public static void main(String[] args) {
        new GameController().run();
    }
}
